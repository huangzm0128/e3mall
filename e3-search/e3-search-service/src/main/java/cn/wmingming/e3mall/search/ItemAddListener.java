package cn.wmingming.e3mall.search;

import cn.wmingming.e3mall.search.service.SearchItemService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * @author Huangzm
 * @create 2018/06/12 21:27
 * @desc 监听商品新增发过来的消息
 * @Modified By:
 **/
public class ItemAddListener implements MessageListener {
    @Autowired
    private SearchItemService searchItemService;
    @Override
    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;
            String id = textMessage.getText();
            //等0.1s，预防还未来得急写入数据库
            Thread.sleep(100);
            searchItemService.addDocument(Long.valueOf(id));
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
