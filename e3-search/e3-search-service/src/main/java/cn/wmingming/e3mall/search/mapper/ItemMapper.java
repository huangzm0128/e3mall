package cn.wmingming.e3mall.search.mapper;

import cn.wmingming.common.pojo.SearchItem;

import java.util.List;

/**
 * @author Huangzm
 * @create 2018/05/25 1:25
 * @desc
 * @Modified By:
 **/
public interface ItemMapper {
    List<SearchItem> getItemList();
    SearchItem getItemById(long id);
}
