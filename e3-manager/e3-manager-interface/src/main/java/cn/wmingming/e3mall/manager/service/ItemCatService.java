package cn.wmingming.e3mall.manager.service;

import cn.wmingming.common.pojo.EasyUITreeNode;

import java.util.List;

public interface ItemCatService {
    List<EasyUITreeNode> getItemList(long parentId);
}
