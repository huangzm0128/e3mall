package cn.wmingming.e3mall.search.service.Impl;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.SearchItem;
import cn.wmingming.common.pojo.SearchResult;
import cn.wmingming.e3mall.search.dao.SolrQueryDao;
import cn.wmingming.e3mall.search.mapper.ItemMapper;
import cn.wmingming.e3mall.search.service.SearchItemService;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @author Huangzm
 * @create 2018/05/25 1:52
 * @desc
 * @Modified By:
 **/
@Service
public class SearchItemServiceImpl implements SearchItemService {
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private SolrServer solrServer;
    @Autowired
    private SolrQueryDao queryDao;


    /**
     * 导入数据到索引库
     * @return
     */
    @Override
    public E3Result importItems() {
        try {
            //查询商品数据
            List<SearchItem> itemList = itemMapper.getItemList();
            //导入索引库
            for (SearchItem item : itemList){
                //创建文档对象
                SolrInputDocument document = new SolrInputDocument();
                //向文档中添加域
                document.addField("id",item.getId());
                document.addField("item_title",item.getTitle());
                document.addField("item_sell_point",item.getSell_point());
                document.addField("item_price",item.getPrice());
                document.addField("item_image", item.getImage());
                document.addField("item_category_name", item.getCategory_name());
                //写入索引库
                solrServer.add(document);
            }
            //提交
            solrServer.commit();
            return E3Result.ok();

        }catch (Exception e){
            e.printStackTrace();
            return E3Result.build(10012,"导入失败");
        }
    }

    /**
     * 从索引库中查询商品
     * @param keyword
     * @param page
     * @param rows
     * @return
     */
    @Override
    public SearchResult search(String keyword, Integer page, int rows)  throws Exception {
        //组装查询条件
        SolrQuery query = new SolrQuery();
        //设置查询的关键词
        query.set("q",keyword);
        //设置分页条件
        query.setStart((page-1) * rows);
        query.setRows(rows);

        //设置默认查询域
        query.set("df","item_keywords");

        //设置高亮
        query.setHighlight(true);
        query.addHighlightField("item_title");
        query.setHighlightSimplePre("<em style=\"color : red\">");
        query.setHighlightSimplePost("</em>");

        //执行查询
        SearchResult searchResult = queryDao.search(query);
        //计算总页数
        long recordCount = searchResult.getRecordCount();
        double totalPages = Math.ceil(recordCount / rows);
        searchResult.setTotalPages((int) totalPages);
        return searchResult;
    }

    @Override
    public E3Result addDocument(long id) {
        E3Result result = E3Result.ok();
        try {
            //查询数据库
            SearchItem item = itemMapper.getItemById(id);
            //添加到索引库
            SolrInputDocument document = new SolrInputDocument();
            document.addField("id", item.getId());
            document.addField("item_title", item.getTitle());
            document.addField("item_sell_point", item.getSell_point());
            document.addField("item_price", item.getPrice());
            document.addField("item_image", item.getImage());
            document.addField("item_category_name", item.getCategory_name());
            solrServer.add(document);
            solrServer.commit();
        } catch (Exception e) {
            e.printStackTrace();
            result = E3Result.build(0003, "添加到索引库失败");
        }
        return result;
    }
}
