package cn.wmingming.e3mall.content.service.impl;

import cn.wmingming.common.jedis.JedisClient;
import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.EasyUIDataGridResult;
import cn.wmingming.common.utils.JsonUtils;
import cn.wmingming.e3mall.content.service.ContentService;
import cn.wmingming.e3mall.mapper.TbContentMapper;
import cn.wmingming.e3mall.pojo.TbContent;
import cn.wmingming.e3mall.pojo.TbContentExample;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Huangzm
 * @create 2018/05/22 13:04
 * @desc
 * @Modified By:
 **/
@Service
public class ContentServiceImpl implements ContentService {
    @Autowired
    private TbContentMapper contentMapper;
    @Autowired
    private JedisClient jedisClient;

    @Value("${CONTENT_LIST}")
    private String CONTENT_LIST;

    @Override
    public List<TbContent> getContentListByCid(long categoryId) {

        //先从redis缓存中查询数据
        try{
            String json = jedisClient.hget(CONTENT_LIST, String.valueOf(categoryId));
            //如果缓存中有直接响应结果
            if (StringUtils.isNoneBlank(json)){
                List<TbContent> contentList = JsonUtils.jsonToList(json, TbContent.class);
                return contentList;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        TbContentExample example = new TbContentExample();
        example.createCriteria().andCategoryIdEqualTo(categoryId);
        List<TbContent> contentList = contentMapper.selectByExample(example);
        //查询数据库后将数据保存到redis缓存
        try{
            String json = JsonUtils.objectToJson(contentList);
            jedisClient.hset(CONTENT_LIST, String.valueOf(categoryId), json);
        }catch (Exception e){
            e.printStackTrace();
        }
        return contentList;
    }

    @Override
    public EasyUIDataGridResult getContentList(long categoryId, int page, int rows) {
        PageHelper.startPage(page,rows);
        TbContentExample example = new TbContentExample();
        List<TbContent> tbContentList;
        if(categoryId != 0){
            example.createCriteria().andCategoryIdEqualTo(categoryId);
            tbContentList = contentMapper.selectByExample(example);
        }else {
            tbContentList = contentMapper.selectByExample(example);
        }
        PageInfo<TbContent> pageInfo = new PageInfo<>(tbContentList);
        EasyUIDataGridResult result = new EasyUIDataGridResult();
        result.setTotal(pageInfo.getTotal());
        result.setRows(tbContentList);
        return result;
    }

    @Override
    public E3Result addContent(TbContent content) {
        Date date = new Date();
        content.setCreated(date);
        content.setUpdated(date);
        contentMapper.insert(content);
        //缓存同步,当增删改时将对应的缓存数据删除(先判断是否存在)
        if(jedisClient.hexists(CONTENT_LIST,content.getCategoryId().toString())){
            jedisClient.hdel(CONTENT_LIST,content.getCategoryId().toString());
        }
        E3Result result = E3Result.ok();
        return result;
    }

    @Override
    public E3Result updateContent(TbContent content) {
        Date date = new Date();
        content.setUpdated(date);
        contentMapper.updateByPrimaryKeySelective(content);
        //缓存同步,当增删改时将对应的缓存数据删除
        if(jedisClient.hexists(CONTENT_LIST,content.getCategoryId().toString())){
            jedisClient.hdel(CONTENT_LIST,content.getCategoryId().toString());
        }
        return E3Result.ok();
    }

    @Override
    public E3Result deleteContent(String[] ids) {
        TbContentExample example = new TbContentExample();
        List<Long>longList = new ArrayList<>();
        for(String id:ids){
            longList.add(Long.valueOf(id));
        }
        example.createCriteria().andIdIn(longList);
        contentMapper.deleteByExample(example);
        //缓存同步,当增删改时将对应的缓存数据删除
        for (String id :ids){
            TbContent content = contentMapper.selectByPrimaryKey(Long.valueOf(id));
            if(jedisClient.hexists(CONTENT_LIST,content.getCategoryId().toString())){
                jedisClient.hdel(CONTENT_LIST,content.getCategoryId().toString());
            }
        }
        return E3Result.ok();
    }
}
