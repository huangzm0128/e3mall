import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

public class test {
    @Test
    public void test1(){
        //System.out.print(-5>>2);
//        Parent parent = new Son();
//        parent.show("xx");
//        int[] ist = new int[]{1,2,3};
//        swap(ist,0,1);
//        System.out.print(ist[0]);
        System.out.print(SeasonEnum.AUTUMN.getValue());
    }

    @Test
    public void test2(){
        String str = "a,b,c,,";
        String[] ary = str.split(",");
        // 预期大于 3，结果是 3
        System.out.println(ary.length);
    }

    @Test
    public void test3(){
        String json = "{\"name\":\"zhangsan\",\"age\":20,\"birthday\":844099200000}";
        /**
         * ObjectMapper支持从byte[]、File、InputStream、字符串等数据的JSON反序列化。
         */
        ObjectMapper mapper = new ObjectMapper();
        User user = null;
        try {
            user = mapper.readValue(json, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(user);
    }

    private static void swap(int[] a, int i, int j) {
        a[i] ^= a[j];
        a[j] ^= a[i];
        a[i] ^= a[j];
    }

    public enum SeasonEnum {
        SPRING(1), SUMMER(2), AUTUMN(3), WINTER(4);
        private int seq;
        SeasonEnum(int seq){
            this.seq = seq;
        }
        public int getValue(){
            return this.seq;
        }
    }
}
