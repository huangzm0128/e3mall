package cn.wmingming.e3mall.cart.service.impl;

import cn.wmingming.common.jedis.JedisClient;
import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.utils.JsonUtils;
import cn.wmingming.e3mall.cart.service.CartService;
import cn.wmingming.e3mall.mapper.TbItemMapper;
import cn.wmingming.e3mall.pojo.TbItem;
import cn.wmingming.e3mall.pojo.TbUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private JedisClient jedisClient;
    @Autowired
    private TbItemMapper itemMapper;
    @Value("${CART_LIST_PRE}")
    private String CART_LIST_PRE;

    @Override
    public E3Result addCart(Long itemId, String userId, Integer num) {
        //数据类型是hash key：用户id field：商品id value：商品信息
        //1 从redis中查看购物车列表是否已经包含该商品
        Boolean hexists = jedisClient.hexists(CART_LIST_PRE + userId, itemId.toString());
        if(!hexists){
            //2 没有包含，从数据库中查询该商品并添加到redis购物车列表中
            TbItem item = itemMapper.selectByPrimaryKey(itemId);
            //设置购物车数据量
            item.setNum(num);
            //取一张图片
            String image = item.getImage();
            if (StringUtils.isNotBlank(image)) {
                item.setImage(image.split(",")[0]);
            }
            jedisClient.hset(CART_LIST_PRE + userId,itemId.toString(),JsonUtils.objectToJson(item));
        }else {
            //3 已经包含，修改该商品的数量
            String itemJson = jedisClient.hget(CART_LIST_PRE + userId, itemId.toString());
            TbItem item = JsonUtils.jsonToPojo(itemJson, TbItem.class);
            item.setNum(item.getNum()+ num);
            jedisClient.hset(CART_LIST_PRE + userId,itemId.toString(),JsonUtils.objectToJson(item));
        }
        //4 返回
        return E3Result.ok();
    }

    @Override
    public List<TbItem> getCartList(String userId) {
        //1 从redis中获取key值为userId的json列表
        List<String> jsonList = jedisClient.hvals(CART_LIST_PRE + userId);
        //2 将jsonList转换为itemList并添加
        ArrayList<TbItem> itemList = new ArrayList<>();
        for (String itemJson: jsonList){
            TbItem item = JsonUtils.jsonToPojo(itemJson, TbItem.class);
            itemList.add(item);
        }
        return itemList;
    }

    @Override
    public E3Result mergeCart(String userId, List<TbItem> itemList) {
        //遍历商品列表
        //把列表添加到购物车。
        //判断购物车中是否有此商品
        //如果有，数量相加
        //如果没有添加新的商品
        for (TbItem item:itemList){
            addCart(item.getId(),userId,item.getNum());
        }
        return E3Result.ok();
    }

    @Override
    public E3Result cartUpdateNum(String userId, Long itemId, Integer num) {
        //获取商品，数量相加
        String itemJson = jedisClient.hget(CART_LIST_PRE + userId, itemId.toString());
        TbItem item = JsonUtils.jsonToPojo(itemJson, TbItem.class);
        item.setNum(item.getNum()+num);
        jedisClient.hset(CART_LIST_PRE+userId,itemId.toString(),JsonUtils.objectToJson(item));
        return E3Result.ok();
    }

    @Override
    public E3Result deleteCartItem(String userId, Long itemId) {
        //删除购物车商品
        jedisClient.hdel(CART_LIST_PRE+userId,itemId.toString());
        return E3Result.ok();
    }
}
