package cn.wmingming.e3mall.sso.service;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.e3mall.pojo.TbUser;

/**
 * @author Huangzm
 * @create 2018/06/22 9:46
 * @desc
 * @Modified By:
 **/
public interface UserService {

    E3Result checkParam(String param, Integer type);
    E3Result register(TbUser tbUser);
    E3Result login(String username, String password);
    E3Result getUserByToken(String token);
}
