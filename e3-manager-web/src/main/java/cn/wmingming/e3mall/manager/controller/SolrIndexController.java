package cn.wmingming.e3mall.manager.controller;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.e3mall.search.service.SearchItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Huangzm
 * @create 2018/05/26 21:45
 * @desc 索引库个管理器
 * @Modified By:
 **/
@Controller
public class SolrIndexController {

    @Autowired
    private SearchItemService searchItemService;
    @RequestMapping(value = "/index/item/import", method = RequestMethod.POST)
    @ResponseBody
    public E3Result importIndex(){
        return searchItemService.importItems();
    }
}
