package cn.wmingming.e3mall.cart.service;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.e3mall.pojo.TbItem;

import java.util.List;

public interface CartService {
    E3Result addCart(Long itemId, String userId,Integer num);
    List<TbItem> getCartList(String userId);
    E3Result mergeCart(String userId,List<TbItem> itemList);
    E3Result cartUpdateNum(String userId, Long itemId, Integer num);
    E3Result deleteCartItem(String userId, Long itemId);
}
