package cn.wmingming.e3mall.search.service;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.SearchResult;

/**
 * @author Huangzm
 * @create 2018/05/25 1:49
 * @desc
 * @Modified By:
 **/
public interface SearchItemService {
    E3Result importItems();
    SearchResult search(String keyword,Integer page,int rows) throws Exception;
    E3Result addDocument(long id);
}
