package cn.wmingming.e3mall.content.service;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.EasyUIDataGridResult;
import cn.wmingming.e3mall.pojo.TbContent;

import java.util.List;

/**
 * @author Huangzm
 * @create 2018/05/22 13:02
 * @desc
 * @Modified By:
 **/
public interface ContentService {
    List<TbContent> getContentListByCid(long categoryId);
    EasyUIDataGridResult getContentList(long categoryId, int page, int rows);
    E3Result addContent(TbContent content);
    E3Result updateContent(TbContent content);
    E3Result deleteContent(String[] ids);
}
