package cn.wmingming.e3mall.manager.service.impl;

import cn.wmingming.common.pojo.EasyUITreeNode;
import cn.wmingming.e3mall.mapper.TbItemCatMapper;
import cn.wmingming.e3mall.pojo.TbItemCat;
import cn.wmingming.e3mall.pojo.TbItemCatExample;
import cn.wmingming.e3mall.manager.service.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemCatServiceImpl implements ItemCatService {
    @Autowired
    private TbItemCatMapper itemCatMapper;
    @Override
    public  List<EasyUITreeNode> getItemList(long parentId) {
        // 装配查询条件
        TbItemCatExample example = new TbItemCatExample();
        TbItemCatExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(parentId);
        //执行查询
        List<TbItemCat> itemCatList = itemCatMapper.selectByExample(example);
        //组装数据
        List<EasyUITreeNode> easyUITreeNodeList = new ArrayList<>();
        for (TbItemCat itemCat : itemCatList){
            EasyUITreeNode node = new EasyUITreeNode();
            node.setId(itemCat.getId());
            node.setText(itemCat.getName());
            node.setState(itemCat.getIsParent()?"closed":"open");
            easyUITreeNodeList.add(node);
        }
        return easyUITreeNodeList;
    }
}
