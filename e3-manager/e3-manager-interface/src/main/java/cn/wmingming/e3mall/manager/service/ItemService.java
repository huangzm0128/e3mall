package cn.wmingming.e3mall.manager.service;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.EasyUIDataGridResult;
import cn.wmingming.e3mall.pojo.TbItem;
import cn.wmingming.e3mall.pojo.TbItemDesc;

import java.util.List;

public interface ItemService {
    TbItem getItemById(long itemId);
    EasyUIDataGridResult getItemList(int page, int rows);
    E3Result addItem(TbItem item, String desc);
    TbItemDesc getItemDesc(long id);
    E3Result updateItem(TbItem item, String desc);
    E3Result deleteItem(String ids);

    /**
     * 下架
     * @param ids
     * @return
     */
    E3Result instockItem(String ids);
    /**
     * 上架
     * @param ids
     * @return
     */
    E3Result reshelfItem(String ids);
}
