package cn.wmingming.e3mall.content.service;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.EasyUITreeNode;

import java.util.List;

/**
 * @author Huangzm
 * @create 2018/05/21 17:40
 * @desc
 * @Modified By:
 **/
public interface ContentCategoryService {
    List<EasyUITreeNode> getContentCategoryList(long parentId);
    E3Result insertContentCategory(Long parentId, String name);
    void updateContentCategory(long id, String name);
    E3Result deleteContentCategory(long id);
}
