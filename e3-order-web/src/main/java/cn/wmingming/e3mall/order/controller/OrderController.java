package cn.wmingming.e3mall.order.controller;

import cn.wmingming.common.utils.CookieUtils;
import cn.wmingming.common.utils.IDUtils;
import cn.wmingming.common.utils.JsonUtils;
import cn.wmingming.e3mall.cart.service.CartService;
import cn.wmingming.e3mall.order.pojo.OrderInfo;
import cn.wmingming.e3mall.order.service.OrderService;
import cn.wmingming.e3mall.pojo.TbItem;
import cn.wmingming.e3mall.pojo.TbOrderItem;
import cn.wmingming.e3mall.pojo.TbOrderShipping;
import cn.wmingming.e3mall.pojo.TbUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

@Controller
public class OrderController {

    @Value("${COOKIES_CART}")
    private String COOKIES_CART;
    @Autowired
    private CartService cartService;
    @Autowired
    private OrderService orderService;

    @RequestMapping("order/order-cart")
    public String showOrderHtml(HttpServletRequest request, HttpServletResponse response, Model model){
        // 从request域中获得用户
        TbUser user = (TbUser) request.getAttribute("user");
        //1 从cookies中获取购物车列表
        String cookiesCartListJson = CookieUtils.getCookieValue(request, COOKIES_CART);
        if(StringUtils.isNoneBlank(cookiesCartListJson)){
            List<TbItem> itemList = JsonUtils.jsonToList(cookiesCartListJson, TbItem.class);
            //2 将cookies中的购物车与服务器端的购物车合并,保存到服务端
            cartService.mergeCart(user.getId().toString(),itemList);
            //3 清空本地购物车
            CookieUtils.deleteCookie(request, response,COOKIES_CART);
        }
        //4 从服务端获取购物车,添加到视图
        List<TbItem> cartList = cartService.getCartList(user.getId().toString());
        model.addAttribute("cartList",cartList);
        return "order-cart";
    }


    @RequestMapping("/order/create")
    public String orderCreate(HttpServletRequest request,OrderInfo orderInfo, Model model){
        //1 补全orderInfo数据
        //1.1 orderId调用orderService服务，它用redis自增生成
        String orderId = orderService.genOrderId();
        orderInfo.setOrderId(orderId);
        //1.2 status 状态：1、未付款，2、已付款，3、未发货，4、已发货，5、交易成功，6、交易关闭
        orderInfo.setStatus(1);
        //1.3 createTime
        orderInfo.setCreateTime(new Date());
        //1.4 userId
        TbUser user = (TbUser) request.getAttribute("user");
        orderInfo.setUserId(user.getId());
        for (TbOrderItem orderItem:orderInfo.getOrderItems()){
            //1.5 orderItems里的id
            orderItem.setId(orderService.genOrderItemId());
            //1.6 orderItems里的orderId
            orderItem.setOrderId(orderId);
        }
        //1.7 orderShipping的orderId
        TbOrderShipping orderShipping = orderInfo.getOrderShipping();
        orderShipping.setOrderId(orderId);
        //1.8 orderShipping的create
        orderShipping.setCreated(new Date());
        //2 调用orderService服务插入orderInfo数据
        orderService.insertOrderInfo(orderInfo);
        model.addAttribute("orderId",orderId);
        model.addAttribute("payment",orderInfo.getPayment());
        return "success";
    }
}
