package cn.wmingming.e3mall.order.service;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.e3mall.order.pojo.OrderInfo;

public interface OrderService {
    String genOrderId();
    String genOrderItemId();
    E3Result insertOrderInfo(OrderInfo orderInfo);
}
