package cn.wmingming.e3mall.manager.controller;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.EasyUIDataGridResult;
import cn.wmingming.e3mall.content.service.ContentService;
import cn.wmingming.e3mall.pojo.TbContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Huangzm
 * @create 2018/05/22 13:12
 * @desc
 * @Modified By:
 **/
@Controller
public class ContentController {
    @Autowired
    private ContentService contentService;

    @RequestMapping("/content/query/list")
    @ResponseBody
    public EasyUIDataGridResult getContentList(long categoryId, int page, int rows){
        return contentService.getContentList(categoryId,page,rows);
    }

    @RequestMapping("/content/save")
    @ResponseBody
    public E3Result addContent(TbContent content){
        return contentService.addContent(content);
    }

    @RequestMapping("/content/edit")
    @ResponseBody
    public E3Result updateContent(TbContent content){
        return contentService.updateContent(content);
    }

    @RequestMapping("/content/delete")
    @ResponseBody
    public E3Result deleteContent(String ids){
        String[] strings = ids.split(",");
        return contentService.deleteContent(strings);
    }
}
