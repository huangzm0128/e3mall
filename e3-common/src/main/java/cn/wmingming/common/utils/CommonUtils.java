package cn.wmingming.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtils {

    public static String getNowDate(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }
}
