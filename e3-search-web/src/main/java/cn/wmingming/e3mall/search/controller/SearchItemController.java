package cn.wmingming.e3mall.search.controller;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.SearchResult;
import cn.wmingming.e3mall.search.service.SearchItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Huangzm
 * @create 2018/05/25 8:28
 * @desc 搜索商品控制器
 * @Modified By:
 **/
@Controller
public class SearchItemController {
    @Autowired
    private SearchItemService searchItemService;
    @Value("${SEARCH_ROWS}")
    private int SEARCH_ROWS;
    @RequestMapping("/search")
    public String search(String keyword, @RequestParam(defaultValue = "1") Integer page, Model model) throws Exception{
        //get请求乱码解决
        String keywordStr = new String(keyword.getBytes("iso8859-1"),"utf-8");
        SearchResult searchResult = searchItemService.search(keywordStr, page, SEARCH_ROWS);
        model.addAttribute("query",keywordStr);
        model.addAttribute("page",page);
        model.addAttribute("totalPages",searchResult.getTotalPages());
        model.addAttribute("recordCount",searchResult.getRecordCount());
        model.addAttribute("itemList",searchResult.getItemList());
        return "search";
    }
}