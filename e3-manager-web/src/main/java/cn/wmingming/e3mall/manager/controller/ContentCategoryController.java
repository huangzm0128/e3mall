package cn.wmingming.e3mall.manager.controller;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.EasyUITreeNode;
import cn.wmingming.e3mall.content.service.ContentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Huangzm
 * @create 2018/05/21 18:08
 * @desc 内容分类控制器
 * @Modified By:
 **/
@Controller
public class ContentCategoryController {

    @Autowired
    private ContentCategoryService contentCategoryService;

    /**
     * 获得内容分类
     * @param parentId
     * @return
     */
    @RequestMapping("/content/category/list")
    @ResponseBody
    public List<EasyUITreeNode> getContentCategoryList(@RequestParam(value = "id", defaultValue = "0") long parentId){
        return contentCategoryService.getContentCategoryList(parentId);
    }

    @RequestMapping(value = "/content/category/create", method = RequestMethod.POST)
    @ResponseBody
    public E3Result insertContentCategory(Long parentId, String name){
        return contentCategoryService.insertContentCategory(parentId,name);
    }

    @RequestMapping(value = "/content/category/update", method = RequestMethod.POST)
    public void updateContentCategory(Long id, String name){
       contentCategoryService.updateContentCategory(id,name);
    }

    @RequestMapping(value = "/content/category/delete/", method = RequestMethod.POST)
    @ResponseBody
    public E3Result deleteContentCategory(Long id){
        return contentCategoryService.deleteContentCategory(id);
    }
}
