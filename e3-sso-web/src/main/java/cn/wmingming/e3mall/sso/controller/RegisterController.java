package cn.wmingming.e3mall.sso.controller;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.e3mall.pojo.TbUser;
import cn.wmingming.e3mall.sso.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * @author Huangzm
 * @create 2018/06/22 8:42
 * @desc 注册控制器
 * @Modified By:
 **/
@Controller
public class RegisterController {
    @Autowired
    private UserService userService;

    @RequestMapping("/page/register")
    public String showRegister(){
        return "register";
    }

    @RequestMapping("/user/check/{param}/{type}")
    @ResponseBody
    public E3Result checkParam(@PathVariable String param, @PathVariable Integer type){
        return userService.checkParam(param,type);
    }

    @RequestMapping("/user/register")
    @ResponseBody
    public E3Result registerUser(TbUser tbUser){
        return userService.register(tbUser);
    }
}
