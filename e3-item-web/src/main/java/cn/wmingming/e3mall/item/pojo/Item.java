package cn.wmingming.e3mall.item.pojo;

import cn.wmingming.e3mall.pojo.TbItem;

/**
 * @author Huangzm
 * @create 2018/06/13 12:55
 * @desc 对TbItem进行扩展
 * @Modified By:
 **/
public class Item extends TbItem {

    public Item(TbItem tbItem){
        this.setId(tbItem.getId());
        this.setCid(tbItem.getCid());
        this.setBarcode(tbItem.getBarcode());
        this.setCreated(tbItem.getCreated());
        this.setImage(tbItem.getImage());
        this.setNum(tbItem.getNum());
        this.setPrice(tbItem.getPrice());
        this.setSellPoint(tbItem.getSellPoint());
        this.setStatus(tbItem.getStatus());
        this.setTitle(tbItem.getTitle());
        this.setUpdated(tbItem.getUpdated());
    }

    /**
     *
     * @return 返回图片数组
     */
    public String[] getImages(){
        if(this.getImage() != null && !"".equals(this.getImage())){
            String[] split = this.getImage().split(",");
            return split;
        }else {
            return null;
        }
    }
}
