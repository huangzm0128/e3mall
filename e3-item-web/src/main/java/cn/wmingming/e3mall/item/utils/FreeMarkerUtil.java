package cn.wmingming.e3mall.item.utils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * @author Huangzm
 * @create 2018/06/20 13:01
 * @desc 用于生成静态页面类
 * @Modified By:
 **/
public class FreeMarkerUtil {
    public static void generateStaticHtml(Map map, FreeMarkerConfigurer freeMarkerConfigurer,String outputFile,String templateName){
        try {
            //1.从spring中获取freemarkerConfigurer对象
            //2.从freemarkerConfigurer中获取configuration对象
            Configuration configuration = freeMarkerConfigurer.getConfiguration();
            //3.使用configuration获取Template对象
            Template template = configuration.getTemplate(templateName);
            //4.创建数据集
            //5.创建文件输出的Writer对象
            Writer out = new FileWriter(new File(outputFile));
            //6.调用template的process方法生成文件
            template.process(map,out);
            //7.关闭资源
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }


    }
}
