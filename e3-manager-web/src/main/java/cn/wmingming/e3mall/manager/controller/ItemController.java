package cn.wmingming.e3mall.manager.controller;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.EasyUIDataGridResult;
import cn.wmingming.e3mall.pojo.TbItem;
import cn.wmingming.e3mall.manager.service.ItemService;
import cn.wmingming.e3mall.pojo.TbItemDesc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ItemController {
    @Autowired
    private ItemService itemService;
    @RequestMapping("item/{itemId}")
    @ResponseBody
    public TbItem getItemById(@PathVariable long itemId){
        return itemService.getItemById(itemId);
    }


    /**
     * 查询商品列表
     * @param page 当前页
     * @param rows 每页显示的条数
     * @return EasyUIDataGridResult
     */
    @RequestMapping("item/list")
    @ResponseBody
    public EasyUIDataGridResult getItemList(Integer page, Integer rows){
        return itemService.getItemList(page,rows);
    }

    @RequestMapping(value = "/item/save", method = RequestMethod.POST)
    @ResponseBody
    public E3Result addItem(TbItem item, String desc){
        return itemService.addItem(item,desc);
    }

    @RequestMapping("/item/query/item/desc/{id}")
    @ResponseBody
    public E3Result getItemDesc(@PathVariable("id") long id){
        TbItemDesc itemDesc = itemService.getItemDesc(id);
        E3Result ok = E3Result.ok();
        ok.setData(itemDesc);
        return ok;
    }

    @RequestMapping("/item/update")
    @ResponseBody
    public E3Result getItemDesc(TbItem item, String desc){
        return itemService.updateItem(item,desc);
    }
    @RequestMapping("/item/delete")
    @ResponseBody
    public E3Result deleteItem(String ids){
        return itemService.deleteItem(ids);
    }
    @RequestMapping("/item/instock")
    @ResponseBody
    public E3Result instockItem(String ids){
        return itemService.instockItem(ids);
    }
    @RequestMapping("/item/reshelf")
    @ResponseBody
    public E3Result reshelfItem(String ids){
        return itemService.reshelfItem(ids);
    }

}
