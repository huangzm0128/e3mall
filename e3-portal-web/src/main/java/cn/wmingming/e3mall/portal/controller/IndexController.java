package cn.wmingming.e3mall.portal.controller;

import cn.wmingming.common.pojo.EasyUIDataGridResult;
import cn.wmingming.e3mall.content.service.ContentService;
import cn.wmingming.e3mall.pojo.TbContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 *
 */
@Controller
public class IndexController {
    @Value("${INDEX_UP_MIDDLE_AD}")
    long INDEX_UP_MIDDLE_AD;
    @Value("${INDEX_UP_RIGHT_AD}")
    long INDEX_UP_RIGHT_AD;
    @Value("${INDEX_MIDDLE_MIDDLE_AD}")
    long INDEX_MIDDLE_MIDDLE_AD;

    @Autowired
    private ContentService contentService;

    @RequestMapping("/index")
    public String showIndex(Model model){
        List<TbContent> umList = contentService.getContentListByCid(INDEX_UP_MIDDLE_AD);
        List<TbContent> urList = contentService.getContentListByCid(INDEX_UP_RIGHT_AD);
        List<TbContent> mmList = contentService.getContentListByCid(INDEX_MIDDLE_MIDDLE_AD);
        model.addAttribute("umList",umList);
        model.addAttribute("urList",urList);
        model.addAttribute("mmList",mmList);
        return "index";
    }
}
