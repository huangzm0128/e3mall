package cn.wmingming.e3mall.content.service.impl;

import cn.wmingming.common.pojo.E3Result;
import cn.wmingming.common.pojo.EasyUITreeNode;
import cn.wmingming.e3mall.content.service.ContentCategoryService;
import cn.wmingming.e3mall.mapper.TbContentCategoryMapper;
import cn.wmingming.e3mall.pojo.TbContentCategory;
import cn.wmingming.e3mall.pojo.TbContentCategoryExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Huangzm
 * @create 2018/05/21 17:50
 * @desc
 * @Modified By:
 **/
@Service
public class ContentCategoryServiceImpl implements ContentCategoryService {

    private int NORMAL = 1;
    private int DELETE = 2;
    @Autowired
    private TbContentCategoryMapper contentCategoryMapper;
    @Override
    public List<EasyUITreeNode> getContentCategoryList(long parentId) {

        //查询条件
        TbContentCategoryExample example = new TbContentCategoryExample();
        example.createCriteria().andParentIdEqualTo(parentId).andStatusEqualTo(NORMAL);
        //执行查询
        List<TbContentCategory> contentCategoryList = contentCategoryMapper.selectByExample(example);
        //组装List<EasyUITreeNode>
        List<EasyUITreeNode> treeNodeList = new ArrayList<>();
        EasyUITreeNode node;
        for (TbContentCategory contentCategory : contentCategoryList){
            node = new EasyUITreeNode();
            node.setId(contentCategory.getId());
            node.setText(contentCategory.getName());
            node.setState(contentCategory.getIsParent()?"closed":"open");
            treeNodeList.add(node);
        }
        return treeNodeList;
    }

    @Override
    public E3Result insertContentCategory(Long parentId, String name) {
        //组装 TbContentCategory
        TbContentCategory category = new TbContentCategory();
        category.setParentId(parentId);
        category.setName(name);
        //状态。可选值:1(正常),2(删除)
        category.setStatus(1);
        category.setIsParent(false);
        //排列序号，表示同级类目的展现次序，如数值相等则按名称次序排列。取值范围:大于零的整数
        category.setSortOrder(1);
        Date date = new Date();
        category.setCreated(date);
        category.setUpdated(date);
        //保存category
        contentCategoryMapper.insert(category);
        TbContentCategory parent = contentCategoryMapper.selectByPrimaryKey(parentId);
        //判断category的父节点的isParent属性
        if(!parent.getIsParent()){
            parent.setIsParent(true);
            //更新父节点
            contentCategoryMapper.updateByPrimaryKey(parent);
        }
        //返回e3Result
        E3Result result = E3Result.ok(category);

        return result;
    }

    @Override
    public void updateContentCategory(long id, String name) {
        TbContentCategory category = new TbContentCategory();
        category.setId(id);
        category.setName(name);
        category.setUpdated(new Date());
        contentCategoryMapper.updateByPrimaryKeySelective(category);
    }

    @Override
    public E3Result deleteContentCategory(long id) {
        //判断该节点是否存在子节点，存在子节点不可以删除
        TbContentCategoryExample example = new TbContentCategoryExample();
        example.createCriteria().andParentIdEqualTo(id).andStatusEqualTo(NORMAL);
        int count = contentCategoryMapper.countByExample(example);
        if(count>0){
            E3Result result = E3Result.build(10021,"该节点含有子节点，不能删除。");
            return result;
        }else {
            TbContentCategory category = contentCategoryMapper.selectByPrimaryKey(id);
            //状态。可选值:1(正常),2(删除)
            category.setStatus(DELETE);
            //执行删除
            contentCategoryMapper.updateByPrimaryKeySelective(category);
            //判断父节点是否还存在其它子节点，若不存在，修改isParent属性为false
            TbContentCategoryExample parentExample = new TbContentCategoryExample();
            parentExample.createCriteria().andStatusEqualTo(NORMAL).andParentIdEqualTo(category.getParentId());
            int parentCount = contentCategoryMapper.countByExample(parentExample);
            if(parentCount == 0){
                TbContentCategory parentCategory = new TbContentCategory();
                parentCategory.setId(category.getParentId());
                parentCategory.setIsParent(false);
                contentCategoryMapper.updateByPrimaryKeySelective(parentCategory);
            }
            E3Result result = E3Result.ok();
            return result;
        }
    }
}
